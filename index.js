const express = require("express")
const session = require("express-session")
const app = express()
const sessionConfig = {
    secret: "s3cr3t",
    resave: false,
    saveUninitialized: false,
}

app.set("view engine", "ejs")
app.use(session(sessionConfig))
app.use(express.urlencoded())

const authorized = (req, res, next) => {
    if (!req.session.isAuthenticated) {
        return res.redirect("/login")
    }

    next()
}

app.get("/", authorized, (req, res) => {
    let user = req.session.user
    res.render("index", {
        user: user
    })
})

app.post("/login", (req, res) => {
    let { username, password } = req.body
    if (username == "admin" && password == "password") {
        req.session.isAuthenticated = true
        req.session.user = {name: username}
        return res.redirect("/")
    }

    res.redirect("/login")
})

app.get("/login", (req, res) => {
    res.render("login", {})
})

app.get("/logout", (req, res) => {
    req.session.destroy()
    res.redirect("/login")
})

app.listen(8000, () => console.log(`listening on port 8000`))
